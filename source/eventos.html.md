---
title: Eventos
layout: page
cover: rubyconf-2017.jpg
---

**Iniciativas para Mulheres**

- [Rails Girls](http://railsgirls.com/saopaulo.html)
- [Ruby Ladies](https://www.meetup.com/Ruby-Ladies/)
- [Progra{m}aria](https://www.programaria.org/)

----

**São Paulo**

- [Ruby Conf Brasil](https://eventos.locaweb.com.br/proximos-eventos/rubyconf-brasil-2018/)
- [GURU SP](https://gurusp.org/encontros)
- [Rubyritas](https://www.meetup.com/Rubyritas/)
- [Ruby Hanami SP](https://www.meetup.com/hanamirb_sp/)

----

**Florianópolis**

- [RubyFloripa](https://www.meetup.com/rubyfloripa/)

----

**Sorocaba**

- [GURU Sorocaba](https://www.meetup.com/GURU-Sorocaba/)

----

**Recife**

- [Tropical Conf](https://tropicalconf.com/)


----

Você conhece outro evento ou meetup sobre ruby? Faça um [fork](https://gitlab.com/rubyonrailsbrasil/rubyonrailsbrasil.com.br) e adicione na lista.
